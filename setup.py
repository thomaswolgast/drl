""" Done with https://towardsdatascience.com/create-your-custom-python-package-that-you-can-pip-install-from-your-git-repository-f90465867893"""

from setuptools import setup, find_packages

with open("README.md", "r", encoding="utf-8") as fh:
    long_description = fh.read()

setup(
    name='drl',
    version='0.0.1',
    author='Thomas Wolgast',
    author_email='thomas.wolgast@uol.de',
    description='Some implementations of DRL base algorithms',
    long_description=long_description,
    long_description_content_type="text/markdown",
    packages=find_packages(include=['drl', 'drl.*']),
    url='https://gitlab.uni-oldenburg.de/lazi4122/drl',
    license='MIT',
    install_requires=[
        'numpy==1.22.4',
        'matplotlib==3.4.0',
        'torch',
        'gymnasium[mujoco]==0.29.0',
        'pettingzoo==1.14.0',
    ],
)
