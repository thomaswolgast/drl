""" Special variants of DDPG for 1-step environment (done always True). """


import numpy as np
import torch

from drl.shared_code.exploration import GaussianNoise
from drl.ddpg import Ddpg


class Ddpg1Step(Ddpg):
    """ Lots of classic optimization problems can be formulated as 1-step
    RL problems, which allows some simplifications in the DRL algos. For
    example no target networks are required and Q=targets=rewards. """

    def _learn(self, obss, acts, rewards, next_obss, dones):
        """ No target updates needed. """
        self._train_critic(obss, acts, rewards, next_obss, dones)
        self._train_actor(obss, acts, rewards, next_obss, dones)

    def _compute_targets(self, next_obss, dones, rewards):
        """ Targets equal the reward in this setting. """
        return rewards


class Ddpg1StepSpecial(Ddpg1Step):
    """ The previous Ddpg1Step algo only speeds up training but does not change
    performance at all. This algo explores possibilities how to improve
    performance in 1-step RL problems.

    # TODO: Not only test multiple actions with critic but actually optimize!
    """

    def __init__(self, env, n_samples=10, noise_std_dev=0.2, *args, **kwargs):
        self.n_samples = n_samples
        super().__init__(env, noise_std_dev=noise_std_dev, *args, **kwargs)

    @torch.no_grad()
    def act(self, obs):
        """ Use actor to create actions and add noise for exploration. """
        if self.memory.memory_counter < self.start_train:
            # TODO: Isnt this wrong? It should be own action space right?
            act = self.env.action_space.sample()  # np.random.sample(n) * 2 -1
            return act

        # Idea: Sample multiple actions from actor and evaluate them with critic
        # TODO: Noise should be higher than for standard DDPG (but compare with same for fairness)
        obs_tensor = torch.tensor(obs, dtype=torch.float).to(self.device)
        action = self.actor.forward(obs_tensor).cpu()

        n_actions = action.expand(self.n_samples, -1)
        n_actions += self.noise()

        # Evaluate all actions with the critic
        exp_rewards = self.critic(
            obs_tensor.expand(self.n_samples, -1),
            n_actions.float().to(self.device)).cpu()

        action = n_actions[exp_rewards.argmax()].numpy()

        return np.clip(action, self.min_range, 1)

    def _init_noise(self, std_dev):
        self.noise = GaussianNoise((self.n_samples, self.n_act), std_dev)
