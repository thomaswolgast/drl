TODO
* Object oriented implementation where the extensions inherit from their base
algorithms so that differences get obvious. Also combinations of extensions are
easily possible this way.

# Installation
Clone the repository and run `pip install -e .` in an virtual environment. The
code is tested for python 3.8 under Linux. (newer version will probably not work)

# Included DRL algorithms
* Base DQN
* DQN extension: Noisy Networks
* DQN extension: N-step DQN
* DQN extension: Double DQN
* DQN extension: Prioritized Experience Replay

* Base DDPG
* DDPG extension: TD3
* DDPG extension: SAC

* MADDPG
* M3DDPG

* Reinforce

# Supported environments:
All open AI gym and pybullet environments can be used by simply using their
environment name as argument, e.g. `--environment CartPoleContinuousBulletEnv-v0`.
All pettingzoo multi-agent RL environments can be used by TODO... Further you
can use any local custom environment that uses the gym API with
`--environment "path.to.env:ClassName"`.

# How to run a simple experiment:
`python drl/experiment.py --agent Td3 --environment Pendulum-v1`

or more verbose with custom environments/algorithms

`python drl/experiment --agent "import.path.to.algo:ClassName" --environment
"import.path.to.env:ClassName" --hyper "{'start_train': 1000}" --num 1 --steps 10000 --store`
(note that dots are used for the path, as in python imports)
